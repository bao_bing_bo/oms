#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@version: ??
@author: rongguo
@license: Apache Licence 
@contact: rongo.wei@gmail.com
@software: PyCharm
@project: oms
@file: forms.py
@time: 17-4-22 下午4:18
"""
from django import forms
from assets.models import *


class IdcModelForm(forms.ModelForm):
    class Meta:
        model = IDC
        fields = ['name', "bandwidth", "operator", 'contacts', 'phone', 'address', 'network', 'comment']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
            'bandwidth': forms.TextInput(attrs={'class': 'form-control'}),
            'operator': forms.TextInput(attrs={'class': 'form-control'}),
            'contacts': forms.TextInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'network': forms.Textarea(attrs={'class': 'form-control', 'placeholder': '192.168.1.0/24\n192.168.2.0/24'}),
            'comment': forms.TextInput(attrs={'class': 'form-control'})
        }


class AssetGroupModelForm(forms.ModelForm):
    class Meta:
        model = AssetGroup
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
            'comment': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Comment'}),
        }


# todo: ModelForm
class AssetModelForm(forms.ModelForm):
    class Meta:
        model = Asset
        fields = '__all__'
        widgets = {
            'hostname': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name'}),
            'ip': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ip'}),
            'other_ip': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'other_ip'}),
            'remote_ip': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'remote_ip'}),
            'mac': forms.TextInput(attrs={'class': 'form-control'}),
            'port': forms.TextInput(attrs={'class': 'form-control'}),
            'brand': forms.TextInput(attrs={'class': 'form-control'}),
            'cpu': forms.TextInput(attrs={'class': 'form-control'}),
            'cpu_nums': forms.TextInput(attrs={'class': 'form-control'}),
            'memory': forms.TextInput(attrs={'class': 'form-control'}),
            'disk': forms.TextInput(attrs={'class': 'form-control'}),
            'system_type': forms.TextInput(attrs={'class': 'form-control'}),
            'system_version': forms.TextInput(attrs={'class': 'form-control'}),
            'system_arch': forms.TextInput(attrs={'class': 'form-control'}),
            'number': forms.TextInput(attrs={'class': 'form-control'}),
            'sn': forms.TextInput(attrs={'class': 'form-control'}),
            'cabinet': forms.TextInput(attrs={'class': 'form-control'}),
            'position': forms.TextInput(attrs={'class': 'form-control'}),

        }
