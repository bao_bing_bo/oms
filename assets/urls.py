#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
@version: ??
@author: rongguo
@license: Apache Licence 
@contact: rongo.wei@gmail.com
@software: PyCharm
@project: oms
@file: urls.py
@time: 17-4-22 下午4:18
"""

from django.conf.urls import url
from assets.views import *

urlpatterns = [
    url(r'^idc/list/$', idc_list, name='idc_list'),
    url(r'^idc/getlist/$', idc_getlist, name='idc_getlist'),
    url(r'^idc/add/$', idc_add, name='idc_add'),
    url(r'^idc/edit/$', idc_edit, name='idc_edit'),
    url(r'^idc/del/$', idc_del, name='idc_del'),
    url(r'^group/list/$', group_list, name='asset_group_list'),
    url(r'^group/getlist/$', group_getlist, name='asset_group_getlist'),
    url(r'^group/add/$', group_add, name='asset_group_add'),
    url(r'^group/edit/$', group_edit, name='asset_group_edit'),
    url(r'^group/del/$', group_del, name='asset_group_del'),
    url(r'^asset/list/$', asset_list, name='asset_list'),
    url(r'^asset/getlist/$', asset_getlist, name='asset_getlist'),
    url(r'^asset/add/$', asset_add, name='asset_add'),
    url(r'^asset/del/$', asset_del, name='asset_del'),
    # url(r'^assets/edit/(?P<id>\d*)/', asset_edit),
    url(r'^asset/edit/$', asset_edit, name='asset_edit'),
    url(r'^asset/detail/$', asset_detail, name='asset_detail'),
    url(r'^asset/get_server_info/$', get_server_info, name='get_server_info'),

]
