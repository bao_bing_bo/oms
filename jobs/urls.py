#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url
from jobs.views import *

urlpatterns = [
    url(r'^remote_exec/$', remote_exec, name='remote_exec'),
    url(r'^fast_execute_script/$', fast_execute_script, name='fast_execute_script'),
    url(r'^fast_push_file/$', fast_push_file, name='fast_push_file'),
    url(r'^job_list/$', job_list, name='job_list'),
    url(r'^new_task/$', new_task, name='new_task'),
    url(r'^crontab_task_list/$', crontab_task_list, name='crontab_task_list'),
    url(r'^task_instance_list/$', task_instance_list, name='task_instance_list'),
    url(r'^chosen_server/$', chosen_server, name='chosen_server'),
    url(r'^new_crontab_task/$', new_crontab_task, name='new_crontab_task'),
    url(r'^upload_demo/$', upload_demo, name='upload_demo')
]
