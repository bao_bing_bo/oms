#!/usr/bin/env python
# encoding: utf-8

"""
@version: v1.0
@author: weirongguo
@license: Apache Licence 
@contact: rongo.wei@gmail.com
@blog: http://www.wilean.com
@software: PyCharm
@project: oms
@file: git_api.py
@time: 2017-01-11 下午5:17
"""

from git import *


# 初始仓库对象
# repo = Repo(repo_path)
# assert repo.bare == False

# 创建裸库
# repo = Repo.init(repo_path, bare=True)
# assert repo.bare == True

# 克隆和初始化一个新的仓库
# cloned_repo = repo.clone(to/this/path)
# new_repo = repo.init(path/for/new/repo)


class git_helper(object):
    def __init__(self, repo_path):
        self.__repo = Repo.init(repo_path)

    def clone_repo(self, to_path):
        cloned_repo = self.__repo.clone(to_path)
