#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url
from configs.views import *

urlpatterns = [
    url(r'^key/list/$', key_list, name='key_list'),
    url(r'^key/get_key_list/$', get_accepted_key, name='get_accepted_key'),
    url(r'^key/get_disallow_list/$', get_unaccepted_key, name='get_unaccepted_key'),
    url(r'^key/delete_key/$', delete_key, name='delete_key'),
    url(r'^key/accept_key$', accept_key, name='accept_key'),
]
