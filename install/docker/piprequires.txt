Django==1.11.1
#psutil==5.2.2
requests==2.13.0
Jinja2==2.9.6
PyYAML==3.12
PyMySQL==0.7.11
celery==4.0.2
pillow==4.1.0